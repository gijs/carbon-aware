import requests
import pprint
from settings import ELECTRICITY_MAPS_API_KEY

def get_power_breakdown (lat, lon):
    api_endpoint = "https://api-access.electricitymaps.com/free-tier/power-breakdown/latest"

    r = requests.get(api_endpoint, {
        'lat': lat,
        'lon': lon
    }, headers={
      'auth-token': ELECTRICITY_MAPS_API_KEY
    })

    if r.status_code == 200:
       return r.json()
    else:
       return None

if __name__ == '__main__':
    from geo_ip import geolocate_ip, get_public_ip
    ip_address = get_public_ip()
    if ip_address:
      print("Found public IP address ", ip_address)
      location = geolocate_ip(ip_address)

      if location:
        print("Found location lat: {}, long: {}".format(location['lat'], location['lon']))
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(get_power_breakdown(location['lat'], location['lon']))
      else:
        print("Could not geolocate IP address.")
    else:
       print("Could not find public IP address.")