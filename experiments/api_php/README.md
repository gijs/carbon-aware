# Carbon Aware PHP-API

PHP implementation of the Carbon Aware API.

The script will lookup the location and power breakdown for the location of client requesting the script, and for the url in the Origin or Referer header. If none is set it will use the IP-address of the server itself.

## Installation

The script does not have dependencies, but requires `file_get_contents` to be [allowed to read URLS](https://www.php.net/manual/en/filesystem.configuration.php#ini.allow-url-fopen).

Place the script in a publicly accessible folder.


## Configuration

If necessary the URL's for API services can be adjusted.


## Client and server localisation
The API consists of two parts, client side code which can easily be included in any web page and the server side code which interacts with API's providing geolocalisation services and information on the current energy mix. The API is designed to be used as a service and can be used by including the client side code in a webpage whithout the need to install server side code.

To determine the type of energy used in a request the API detects the location of the both device requesting the orginal webpage and of the server hosting the original webpage; the website which included the client side code.

The address of the client device is detected on the API-server by reading the IP address of the device which sent the request. The location is then looked up in geolocalisation databases. When the client device connects through a VPN, or a cellular network the precision of the localisation will be influenced.

To detect the location of the server hosting the original website the API reads the 'Origin' or 'Referer' HTTP headers. These headers provide the hostname of the website which included the client side code. The IP address of the found hostname is then looked up using DNS and a location is determined through the geolocalisation API. The precision of the server localisation can be influenced by the configuration of the original website, e.g.: when it is hosted on a content delivery network. 