import requests

def get_contract_energy ():
  api_endpoint = 'https://opendata.elia.be/api/explore/v2.1/catalog/datasets/ods034/records'
  query = {
    'select': 'sum(dayaheadgenerationschedule) as dayahedgenerationschedule, fuelcode',
    'where': 'datetime >= "2023-10-02T00:00:00+00:00" and datetime < "2023-10-03T00:00:00+00:00"',
    'group_by': 'fuelcode',
    'timezone': 'CET'
  }

  r = requests.get(api_endpoint, params=query)

  if r.status_code == 200:
    return r.json()['results']
  else:
    # Raise exception
    return []

def get_solar_production_forecasts ():
  # Groups solar production per timeslot and sums total forecasted production
  # finds the most recent entry in the database
  api_endpoint = 'https://opendata.elia.be/api/explore/v2.1/catalog/datasets/ods087/records'
  query = {
    'select': 'region, datetime, realtime, mostrecentforecast as forecast',
    'where': 'datetime >= "2023-10-02T11:00:00+00:00" and datetime < "2023-10-02T11:15:00+00:00"',
    'limit': 100,
    'timezone': 'CET'
  }

  r = requests.get(api_endpoint, params=query)
  print(r)
  if r.status_code == 200:
    return r.json()['results']
  else:
    return []

def get_most_recent_solar_production ():
  # Groups solar production per timeslot and sums total forecasted production
  # finds the most recent entry in the database
  api_endpoint = 'https://opendata.elia.be/api/explore/v2.1/catalog/datasets/ods087/records'
  query = {
    'select': 'realtime, mostrecentforecast as forecast',
    'where': 'region = "Belgium" and datetime < now()',
    'order_by': 'datetime desc',
    'limit': 1,
    'offset': 2,
    'timezone': 'CET'
  }

  r = requests.get(api_endpoint, params=query)
  print(r)
  if r.status_code == 200:
    return r.json()['results'][0]
  else:
    return []
  
def get_most_recent_wind_production ():
  # Groups solar production per timeslot and sums total forecasted production
  # finds the most recent entry in the database
  api_endpoint = 'https://opendata.elia.be/api/explore/v2.1/catalog/datasets/ods086/records'
  query = {
    'select': 'sum(realtime) as realtime, sum(mostrecentforecast) as forecast, datetime',
    'where': 'datetime < now()',
    'order_by': 'datetime desc',
    'group_by': 'datetime',
    'limit': 1,
    'offset': 2,
    'timezone': 'CET'
  }

  r = requests.get(api_endpoint, params=query)
  print(r, r.json())
  if r.status_code == 200:
    return r.json()['results'][0]
  else:
    return []

data = get_most_recent_wind_production()

print(data)

data = get_most_recent_solar_production()

print(data)

# data = get_solar_production_forecasts()

# print(data)

# belgium = sum(map(lambda r: r['realtime'], filter(lambda r: r['region'] not in ['Wallonia', 'Flanders', 'Belgium'], data)))

# print(belgium)

# print(data)


# production = get_most_recent_solar_production ()

# print(production)