import requests

# Uses ip-api.com

ip_api_base_url = "http://ip-api.com/json/"

def geolocate_ip (ip):
  print(ip_api_base_url + ip)
  // Return status, country, countryCode, city, lat, lon
  r = requests.get(ip_api_base_url + ip, params={ 'fields': '16595'})

  if r.status_code == 200:
    return r.json()
  else:
    return None
  
def get_public_ip ():
  r = requests.get('https://api.ipify.org')

  if r.status_code == 200:
    return r.text
  else:
    return None

if __name__ == '__main__':
  print(geolocate_ip(get_public_ip()))