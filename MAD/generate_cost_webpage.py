import electricity_maps
import svgwrite
from KWHPerByte import KWHDatacenter, KWHDevice, KWHNetwork
import sys

server_location = {
   'lat': 50.6937,
   'lon': 3.1744
}

client_location = {
   'lat': 50.8511,
   'lon': 4.3443
}

# Weight of mad.brussels/fr in bytes, measured on 12 September 2024
transfer_size = 5008846

server_intensity = electricity_maps.get_carbon_intensity(server_location['lat'], server_location['lon'])

if server_intensity is None:
  sys.stderr.write('generate_cost_webpage.py: Falling back to default server intensity value.\n')
  server_intensity = 59

client_intensity = electricity_maps.get_carbon_intensity(client_location['lat'], client_location['lon'])

if client_intensity is None:
  sys.stderr.write('generate_cost_webpage.py: Falling back to default client intensity value.\n')
  client_intensity = 59

kwh_datacenter = KWHDatacenter(transfer_size)
kwh_network = KWHNetwork(transfer_size)
kwh_device = KWHDevice(transfer_size)

co2_server = kwh_datacenter * server_intensity
co2_client = kwh_device * client_intensity
co2_network = kwh_network * .5 * (client_intensity + server_intensity)

svg_document = svgwrite.Drawing(filename="cost_webpage.svg", size = ("500px", "500px"))

svg_document.add(svg_document.text(
    'visiting the webpage', insert=(250, 90), style='font-size: 45px; text-anchor: middle;'
))


svg_document.add(svg_document.text(
    'mad.brussels/fr', insert=(250, 145), style='font-size: 45px; text-anchor: middle;'
))


svg_document.add(svg_document.text(
    'generates now', insert=(250, 200), style='font-size: 45px; text-anchor: middle;'
))

svg_document.add(svg_document.text(
    '{:.5}'.format(co2_server + co2_network + co2_client), insert=(250, 330), style='font-size: 120px; text-anchor: middle;'
))


svg_document.add(svg_document.text(
    'grams CO2', insert=(250, 405), style='font-size: 45px; text-anchor: middle;'
))

svg_document.save()