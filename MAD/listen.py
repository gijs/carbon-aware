from evdev import InputDevice, categorize, ecodes, list_devices
import subprocess
import sys
import os

target_device_name = sys.argv[1]

actions = {
  'KEY_0': lambda: run_bash_script('plot-spiral.sh'),
  'KEY_1': lambda: run_bash_script('plot-carbon-intensity.sh'),
  'KEY_2': lambda: run_bash_script('plot-cost-webpage.sh')
  # 'KEY_3': lambda: run_bash_script('plot.sh')
}

def run_bash_script (path):
  args = [
    'bash',
    path
  ]

  print(f'Running { path }')

  try:
    # Replace for check_call to keep default stdout & stdin
    return subprocess.check_call(args, stdout=sys.stdout, stderr=sys.stderr,  cwd=os.path.dirname(os.path.realpath(__file__)))
    # return subprocess.check_output(args, stderr=subprocess.STDOUT,).decode()
  except subprocess.CalledProcessError as e:
    print(e)
    return None

def find_device (name):
  for device in [InputDevice(path) for path in list_devices()]:
      if device.name == name:
         return device
    
  return None

def wait_for_key (device):
  # Clear event queue
  while device.read_one() != None:
    pass
  for event in device.read_loop():
    if event.type == ecodes.EV_KEY:
      key_event = categorize(event)
      return key_event.keycode

device = find_device(target_device_name)

if device:
  print(f'Found { device.name } at {device.path}')
  print("Waiting to initiate power plug.")
  key = wait_for_key(device)
  print("Initiating power plug.")
  print(run_bash_script('steam/init.sh'))

  while True:
    print("Waiting for input")
    key = wait_for_key(device)
    
    if key in actions:
      actions[key]()
