#! /bin/bash
source .env

echo "*********************"
date
echo "Plotting Cost Webpage"
echo "Converting SVG"

# Generate spiral based on carbon intensity
$PYTHON generate_cost_webpage.py

# Convert to gcode
bash convert.sh cost_webpage.svg cost_webpage.gcode EMS_extended.svg 

# Init steamer
echo "Steaming for 10 seconds"
bash 'steam/on.sh'
sleep $STEAM_LENGTH  
bash 'steam/off.sh'

# Plot
echo "Plotting"
$PYTHON plot.py $SERIAL cost_webpage.gcode