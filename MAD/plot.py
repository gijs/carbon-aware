"""
  Script to send a gcode file to a plotter

  plot.py serialport gcodefile

"""

import serial
import time
import argparse
import sys

def send_command(ser, command):
  ser.write(str.encode(command)) 

  while True:
    line = ser.readline()
    time.sleep(0.01)
    if line.startswith(b'ok'):
      break

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='Plot gcode file', description='Script to send files to a plotter')
    parser.add_argument('serialport', type=str)
    parser.add_argument('gcode', nargs='?', type=argparse.FileType('r'), default=sys.stdin)

    args = parser.parse_args()

    ser = serial.Serial(args.serialport, 115200)

    time.sleep(2)

    for line in args.gcode.readlines():
            send_command(ser,line)
