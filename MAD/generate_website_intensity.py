import electricity_maps
import svgwrite
from KWHPerByte import KWHDatacenter, KWHDevice, KWHNetwork

server_location = {
   'lat': 50.6937,
   'lng': 3.1744
}

client_location = {
   'lat': 50.8511,
   'lng': 4.3443
}

# Weight of mad.brussels/fr in bytes, measured on 12 September 2024
transfer_size = 5008846

server_intensity_result = electricity_maps.get_carbon_intensity(server_location['lat'], server_location['lon'])

if server_intensity_result:
  server_intensity = server_intensity_result['carbonIntensity']
else:
  server_intensity = 59

client_intensity_result = electricity_maps.get_carbon_intensity(client_location['lat'], client_location['lon'])

if client_intensity_result:
  client_intensity = client_intensity_result['carbonIntensity']
else:
  client_intensity = 59

kwh_datacenter = KWHDatacenter(transfer_size)
kwh_network = KWHNetwork(transfer_size)
kwh_device = KWHDevice(transfer_size)

co2_server = kwh_datacenter * server_intensity
co2_client = kwh_device * client_intensity
co2_network = kwh_network * .5 * (client_intensity + server_intensity)

svg_document = svgwrite.Drawing(filename="cost-website.svg", size = ("500px", "500px"))

svg_document.add(svg_document.text(
    'Visiting mad.brussels/fr', insert=(250, 100), style='font-size: 50px; text-anchor: middle;'
))


svg_document.add(svg_document.text(
    'costs', insert=(250, 175), style='font-size: 50px; text-anchor: middle;'
))

svg_document.add(svg_document.text(
    '{:.4} grams'.format(co2_server + co2_network + co2_client), insert=(250, 350), style='font-size: 150px; text-anchor: middle;'
))


svg_document.add(svg_document.text(
    'gr. CO2 / kWh', insert=(250, 400), style='font-size: 50px; text-anchor: middle;'
))

svg_document.save()