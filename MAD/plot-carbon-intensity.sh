#! /bin/bash
source .env

echo "*********************"
date
echo "Plotting Carbon intensity"
echo "Converting SVG"

# Generate spiral based on carbon intensity
$PYTHON generate_carbon_intensity.py

# Convert to gcode
bash convert.sh carbon-intensity.svg carbon-intensity.gcode EMS_extended.svg 

# Init steamer
echo "Steaming for 10 seconds"
bash 'steam/on.sh'
sleep $STEAM_LENGTH
bash 'steam/off.sh'

# Plot
echo "Plotting"
$PYTHON plot.py $SERIAL carbon-intensity.gcode