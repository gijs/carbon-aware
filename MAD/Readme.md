# Carbon aware at MAD 2024

This folder holds the files to run the installation during exhibition at MAD, 2024.

All files are installed in the folder `/home/carbon-techno/installation/MAD`.

On boot a shell script is started `init.sh` which initializes the Vaydeer keypad and starts a python script `listen.py` which listens to key events. This python script will start the bash scripts that do the actual plotting. After a first key-press it will run the smartplug initialization script. On subsequent keypresses the individual plot scripts will be started.

From left to right:
- Key 0: `plot-spiral.sh`
- Key 1: `plot-carbon-intensity.sh`
- Key 2: `plot-cost-webpage.sh`
- Key 3: `<unassigned>`

The individual bash scripts often follow a similar structure:
1. Run a python script to generate a SVG-file
2. Convert the SVG into gcode (using `convert.sh` or `convert-without-optimization.sh`)
3. Turn the steamer on, wait, turn it off
4. Plot the gcode

## Operation

After the raspberry has booted the first key press on the keypad will start the smartplug initialization script, subsequent keypresses run one of the drawing scripts. 

To start the installation:
- Make sure the raspberry is turned off
- Make sure the smartplug (of the steamer) is plugged out
- Make sure the plotter is plugged in
- Turn on the raspberry and let it boot completely (the green status-led should be off)
- Plug in the smart plug and then press any key on the keypad. The steamer should turn on and turn off almost immediately.

Any next keypresses should start a drawing.

### Debugging

To follow the logging ssh into the raspberry and run:
```
tail -f installation/MAD/log
```
The command will display the last lines of the log and display lines when they are written.

## .env

General variables like the name of the keypad, steam time and locations of binaries like Python, svg2gcode and vpype are controlled through the `.env`-file. Located at `/home/carbon-techno/installation/MAD/.env`

## Autostart on Raspberry

Added following autostart section to `~/.config/wayfire.ini` to start the installation on boot. Both `stdout` and `stderr` are rerouted to `/home/carbon-techno/installation/MAD/log`

```
[autostart]
mad_installation = sudo /home/carbon-techno/installation/MAD/init.sh 1>>/home/carbon-techno/installation/MAD/log 2>&1
```

## Virtual environment

All python requirements were installed in a virtual environment located at `/home/carbon-techno/installation/MAD/venv`.

## Requirements 

### Python

Python requirements are listed in `requirements.txt`.

Install with `pip install -r requirements.txt`.

### vpype (pipx)

[vpype](https://github.com/abey79/vpype) is used to clean up the generated SVG's and to scale them into the plottable size. vpype requires pipx, they can be installed with the following commands.

```
sudo apt update
sudo apt install pipx
pipx ensurepath
pipx install vpype
```

### svg2gcode (rust)

[svg2gcode](https://sameer.github.io/svg2gcode/) converts the svg files to gcode, it requires [rust](https://www.rust-lang.org/).

For the installation rust was installed with this command:
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Then clone the repository of svg2gcode:

```
git clone https://github.com/sameer/svg2gcode.git
```

cd into the repository and build a release binary with

```
cargo build --release --bin svg2gcode
```

The binary should be at: `target/release/svg2gcode`.

For the installation this binary was then copied to: `/home/carbon-techno/installation/MAD/svg2gcode`.
