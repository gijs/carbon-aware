#! /bin/bash
cd /home/carbon-techno/installation/MAD/

source .env

echo ""
echo "*********************"
echo "Starting installation"
echo "*********************"
date
echo "*********************"

echo "Initializing keyboard"
# Initialize the keyboard to make sure events are recognized in the python script
sudo bash init_keyboard.sh

echo "Starting python loop listener"
# Start python script to listen to key input
$PYTHON listen.py "${KEYPAD_DEVICE_NAME}"