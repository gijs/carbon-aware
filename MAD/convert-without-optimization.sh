# Utility script to convert SVG to gcode files
# First the text in the SVG is replaced by paths
# Then the file is cleaned and scaled with vpype
# Finally it transformed to gcode with juicy gcode

source .env

svg_in=$1;
gcode_out=$2;

echo "Converting to gcode"
# ./juicy-gcode "${svg_cleaned}" \
#   -f flavor.yaml \
#   -o $gcode_out

$SVG2GCODE \
  --dpi 96 \
  --tolerance 0.5 \
  --begin "G21 G17 G90 F1000 G00 Z0 G92 X0 Y120 Z0" \
  --end "G0Z0 G0 X0 Y120 Z0" \
  --on "G0Z10" \
  --off "G0Z0" \
  --out "${gcode_out}" \
  --feedrate 1000 \
  "${svg_in}"
