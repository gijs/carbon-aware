#!/usr/bin/env python
# coding: utf-8

import numpy as np
import svgwrite
from noise import pnoise1  # You might need to install this library
import svgwrite
import electricity_maps
import sys

lat = 50.85113089086121
lng = 4.34434795874892

def create_spiral_with_noise(width, height, noise_intensity):
    num_points = 1000  # Number of points in the spiral
    max_radius = (min(width, height) / 2) * 0.95  # Maximum radius of the spiral
    dwg = svgwrite.Drawing('spiral.svg', size=(f'{width}mm', f'{height}mm'), profile='tiny', viewBox=('0 0 100 100'))

    # Create spiral coordinates
    points = []
    for i in range(num_points):
        angle = 10 * np.pi * (i / num_points)  # Number of full rotations, adjust for tighter or looser spiral
        r = max_radius * (i / num_points)  # Radius depends on the point index
        x = width/2  + r * np.cos(angle)
        y = height/2 + r * np.sin(angle)
        # Adding noise
        noise = pnoise1(i / 100.0 * noise_intensity) * (max_radius * 0.1)  # Scale noise effect with radius
        x += noise * np.cos(angle)   # Apply noise along the x-axis
        y += noise * np.sin(angle)  # Apply noise along the y-axis
        points.append((x, y))
    
    # Draw spiral
    dwg.add(dwg.polyline(points[::-1], stroke='black', fill='none', stroke_width='0.1'))
    dwg.save()


carbon_intensity = electricity_maps.get_carbon_intensity(lat, lng)

if not carbon_intensity:
    sys.stderr.write('spiral.py: Falling back to default intensity value.\n')
    carbon_intensity = 70

noise_intensity = min(99, (carbon_intensity / 3))
# Intensity is remapped based on two assumptions
# 1 - noise should be less than 100
# 2 - carbon intensity will never (?) be higher than 300 in Belgium 
#     therefore: (carbon_intensity / 300) * 100 -> carbon_intensity / 3
create_spiral_with_noise(125, 125, noise_intensity)  # 10cm x 10cm image, moderate noise intensity




