#! /bin/bash
source .env

echo "*********************"
date
echo "Plotting Power Grid"
echo "Converting SVG"

# Generate spiral based on carbon intensity
$PYTHON generate_power_grid.py

# Convert to gcode
bash convert.sh powergrid.svg powergrid.gcode EMS_extended.svg 

# Init steamer
echo "Steaming for 10 seconds"
bash 'steam/on.sh'
sleep $STEAM_LENGTH  
bash 'steam/off.sh'

# Plot
echo "Plotting"
$PYTHON plot.py $SERIAL powergrid.gcode