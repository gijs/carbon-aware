#! /bin/bash
source .env

echo "*********************"
date
echo "Plotting spiral"
echo "Converting SVG"

# Generate spiral based on carbon intensity
$PYTHON spiral.py

# Convert to gcode
bash convert-without-optimization.sh spiral.svg spiral.gcode

# Init steamer
echo "Steaming for 10 seconds"
bash 'steam/on.sh'
sleep $STEAM_LENGTH  
bash 'steam/off.sh'

# Plot
echo "Plotting"
$PYTHON plot.py $SERIAL spiral.gcode