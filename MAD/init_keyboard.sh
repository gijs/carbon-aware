#!/bin/bash
source .env

# This script initiates the Vaydeer keyboard
# and is necessary to make python recognize input
FILES=/dev/hidraw*
for f in $FILES
do
  FILE=${f##*/}
  DEVICE="$(cat /sys/class/hidraw/${FILE}/device/uevent | grep HID_NAME | cut -d '=' -f2)"
  if [ "$DEVICE" == "$KEYPAD_DEVICE_NAME" ]
  then
    printf "%s \t %s\n" $FILE "$DEVICE"
    cat /dev/${FILE} > /dev/null &
  fi
done