# Reimplemenation of: https://github.com/thegreenwebfoundation/co2.js/blob/main/src/1byte.js
KWH_PER_BYTE_IN_DC = 7.2e-11

FIXED_NETWORK_WIRED = 4.29e-10
FIXED_NETWORK_WIFI = 1.52e-10
FOUR_G_MOBILE = 8.84e-10

KWH_PER_BYTE_FOR_NETWORK = (FIXED_NETWORK_WIRED + FIXED_NETWORK_WIFI + FOUR_G_MOBILE) / 3

KWH_PER_BYTE_FOR_DEVICES = 1.3e-10

# Average KWH per byte of transferred data
KWH_PER_BYTE = KWH_PER_BYTE_IN_DC + KWH_PER_BYTE_FOR_NETWORK + KWH_PER_BYTE_FOR_DEVICES

# Convenience function to determine amount of energy
# used to retrieve and render a webpage, determined
# through the page weight
def KWH (bytes):
    return bytes * KWH_PER_BYTE

def KWHDatacenter (bytes):
    return bytes * KWH_PER_BYTE_IN_DC

def KWHDevice (bytes):
    return bytes * KWH_PER_BYTE_FOR_DEVICES

def KWHNetwork (bytes):
    return bytes * KWH_PER_BYTE_FOR_NETWORK