Scripts to convert SVG-files to gcode to be plotter.

## convert.sh

Converts an svg drawing to a gcode file. First text is replaced by fonts. Then the file is simplified and placed on an A4-sheet using vpype. Finally the svg is converted to gcode using juicy-gcode.

```
  convert.sh file_in.svg file_out.gcode stroke_font.svg
```

## Requirements
- [tinycss2](https://doc.courtbouillon.org/tinycss2/)
- [lxml](https://lxml.de/)
- [pyserial](https://pyserial.readthedocs.io/en/)
- [vpype](https://github.com/abey79/vpype)
- [juicy-gcode](https://github.com/domoszlai/juicy-gcode)

