"""
  Utility to remove the backgroundframe matplotlib inserted in
  the svg.

  remove_matplot_frame.py svgpathin svgpathout
"""

from lxml import etree as ET
import argparse
import sys

def remove_matplot_frame (svg_in):
  tree = ET.parse(svg_in)
  root = tree.getroot()
  namespace = {'svg': 'http://www.w3.org/2000/svg'}
  patch = root.find('.//svg:g[@id = "patch_1"]', namespace)
  patch.getparent().remove(patch)
  return ET.tostring(root, pretty_print=True, encoding='unicode')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='Replace font', description='Script to replace text and tspan elements in an SVG with paths taken from an SVG stroke font.')
    parser.add_argument('svg_in', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('svg_out', nargs='?', type=argparse.FileType('w'), default=sys.stdout)

    args = parser.parse_args()

    args.svg_out.write(remove_matplot_frame(args.svg_in))
