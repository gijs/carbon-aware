# Utility script to convert SVG to gcode files
# First the text in the SVG is replaced by paths
# Then the file is cleaned and scaled with vpype
# Finally it transformed to gcode with juicy gcode

svg_in=$1;
svg_outlined="${svg_in%.svg}-outlined.svg"
svg_cleaned="${svg_in%.svg}-cleaned.svg"
gcode_out=$2;
font=$3;

echo "Converting text to paths."
python replace_text_with_paths.py --font "${font}" "${svg_in}"  "${svg_outlined}"

echo "Scaling and cleaning SVG"
#linesimplify -t 0.3mm \
vpype read "${svg_outlined}" \
  scaleto 12cm 12cm \
  linemerge -t 0.1mm \
  linesort \
  write --page-size 15cmx15cm --format \
  svg --center "${svg_cleaned}"

echo "Converting to gcode"
./juicy-gcode "${svg_cleaned}" \
  -f flavor.yaml \
  -o $gcode_out

#echo "Cleaning up temporary files"
# rm "${svg_outlined}" "${svg_cleaned}";