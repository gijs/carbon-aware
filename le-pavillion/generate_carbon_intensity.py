from lighthouse import electricity_maps
import math
import random
import svgwrite

lat = 50.47245336194251
lng = 4.862539272711904

"""
    {
      'zone': 'BE', 
      'datetime': '2024-05-30T09:00:00.000Z', 
      'updatedAt': '2024-05-30T09:50:06.514Z', 
      'createdAt': '2024-05-27T09:47:14.823Z', 
      'powerConsumptionBreakdown': {
          'nuclear': 4828, 
          'geothermal': 0, 
          'biomass': 117, 
          'coal': 0, 
          'wind': 708, 
          'solar': 2512, 
          'hydro': 416, 
          'gas': 359, 
          'oil': 6, 
          'unknown': 200, 
          'hydro discharge': 46, 
          'battery discharge': 0
      }, 
      'powerProductionBreakdown': {
          'nuclear': 3395, 
          'geothermal': None, 
          'biomass': 104, 
          'coal': None,
          'wind': 613,
          'solar': 2607,
          'hydro': 0,
          'gas': 387,
          'oil': 0,
          'unknown': 234,
          'hydro discharge': -166,
          'battery discharge': None
      },
      'powerImportBreakdown': {'DE': 0, 'FR': 3411, 'GB': 0, 'LU': 0, 'NL': 0},
      'powerExportBreakdown': {'DE': 692, 'FR': 0, 'GB': 176, 'LU': 148, 'NL': 377},
      'fossilFreePercentage': 94, 
      'renewablePercentage': 41, 
      'powerConsumptionTotal': 9192, 
      'powerProductionTotal': 7340, 
      'powerImportTotal': 3411, 
      'powerExportTotal': 1393, 
      'isEstimated': True, 
      'estimationMethod': 'TIME_SLICER_AVERAGE'
}
"""
# api_result = electricity_maps.get_carbon_intensity(lat, lng)
api_result = True
if api_result:
    intensity = 53 #api_result['carbonIntensity']

    svg_document = svgwrite.Drawing(filename="carbon-intensity.svg", size = ("500px", "500px"))

    svg_document.add(svg_document.text(
        'carbon intensity', insert=(250, 100), style='font-size: 50px; text-anchor: middle;'
    ))

    svg_document.add(svg_document.text(
        intensity, insert=(250, 300), style='font-size: 200px; text-anchor: middle;'
    ))


    svg_document.add(svg_document.text(
        'gr. CO2 / kWh', insert=(250, 400), style='font-size: 50px; text-anchor: middle;'
    ))

    svg_document.save()