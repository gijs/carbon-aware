import datetime
import csv

"""

    Calculate an average usage per hour.
    Count week and weekend days per month to find hours at night and day rate.
    Do a simple division in the end.

"""


with open('consumption-2022.csv') as csvfile:
    reader = csv.reader(csvfile)
    consumption = [(float(day), float(night)) for (day, night) in reader]

start = datetime.date(2022,1,1)
end = datetime.date(2023,1,1)
step = datetime.timedelta(days=1)

date = start

months = []
month = 1

weekdays = 0
weekenddays = 0

while date < end:
    if date.weekday() < 5:
        weekdays += 1
    else:
        weekenddays += 1
    
    date += step
    
    if date.month != month:
        months.append((weekdays, weekenddays))
        month = date.month
        weekdays = 0
        weekenddays = 0

print(months)

for index, (weekdays, weekenddays) in enumerate(months):
    hours_day = weekdays * 16
    hours_night = weekdays * 8 + weekenddays * 24
    consumption_day, consumption_night = consumption[index]

    print(consumption_day/hours_day, consumption_night/hours_night)