import json
import datetime

# settings
#   series
#     data
def load_export (path):
    with open(path, 'r') as h:
        return json.load(h)



def get_production_data (solarweb_export):
    series = solarweb_export['settings']['series'][0]
    production_data = series['data']
    # Some exports are in W, rather than kW
    if series['yAxis'] == 'W':
      production_data = [[d, v*0.001 if v else None] for [d, v] in production_data]
      
    return [ (datetime.datetime.fromtimestamp(timestamp/1000), value) for (timestamp, value) in production_data ]
    