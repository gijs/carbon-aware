import matplotlib.pyplot as plt
import analyze
import solarweb
import datetime
from parse_electricity_maps_csv import load_carbon_intensities
import hourly_consumption
import sys
import os.path

plt.rcParams['svg.fonttype'] = 'none'

def time_to_float (time):
    return (time.hour + time.minute/60) / 24

get_carbon_intensity = load_carbon_intensities('BE_2022_hourly.csv') 

date = datetime.date(day=30, month=5, year=2022)

# matplotlib defines figure size in inch, one inch = 25.4mm
width = 200 / 25.4
height = 100 / 25.4

solar_fig, solar_ax = plt.subplots(figsize=(width, height))
solar_ax.set_ylim([0,10])
# solar_ax.set_yticks([2,4,6,8,10], labels=[2,4,6,8,10], family='AVHershey Simplex')
# solar_ax.set_ylabel('kWh', loc='top', family='AVHershey Simplex')
solar_ax.set_axis_off()
solar_ax.set_frame_on(False)
solar_ax.set_xmargin(0)
solar_ax.set_xticks([.25,.5,.75], labels=['06:00', '12:00', '18:00'], family='AVHershey Simplex')

for (export_date, path) in analyze.discover_exports():
    if export_date == date:
        hourly_consumption = hourly_consumption.get_hourly_consumption(date.month)
        
        solar_data = solarweb.get_production_data(solarweb.load_export(path))
        intensity_data = [(date, get_carbon_intensity(date)) for (date, _) in solar_data]
        x, y = zip(*solar_data)
        
        # analysis = analyze.analyze_production_data(data)
        solar_line = solar_ax.plot([time_to_float(d) for d in x], y)
        solar_line[0].set(color='black', linewidth=.5)
        
if len(sys.argv) > 1:
    path = sys.argv[1]
else:
    path = '.'

solar_fig.savefig(os.path.join(path, 'solar-production.svg'))