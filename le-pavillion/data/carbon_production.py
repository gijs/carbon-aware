import datetime
from .parse_electricity_maps_csv import load_carbon_intensities
import glob
import re
from . import solarweb
from . import hourly_consumption
import os.path

"""
  Returns energy usage in kWh for a given time period at given date.
  In this example usage is linked to assumed opening hours 12:00 - 18:00
"""
def get_usage (timedelta, date):
    (hourly_consumption_day, hourly_consumption_night) = hourly_consumption.get_hourly_consumption(date.month)
    delta_hours = timedelta.total_seconds() / 3600

    if date.weekday () < 5:
        if date.hour < 7 or date.hour > 21:
            return hourly_consumption_night * delta_hours
        else:
            return hourly_consumption_day * delta_hours
        
    else:
        return hourly_consumption_night * delta_hours

get_carbon_intensity = load_carbon_intensities(os.path.join(os.path.dirname(__file__), 'BE_2022_hourly.csv')) 

def calculate_carbon_production_up_to (production_data, cutoff):
    # Production of solar in kWh
    production = 0
    # Time between data points
    timedelta = datetime.timedelta(minutes=5)
    timedelta_hours = timedelta.total_seconds() / 3600

    co2 = 0

    for [date, production_now] in production_data:
        if date < cutoff:
          # Production is in kWh
          if production_now is None:
              production_now = 0

          production_now *= timedelta_hours
                  
          # Assume the panel produces the same amount of energy until the next measurement
          production += production_now

          # Derive 
          usage_now = get_usage(timedelta, date)
          usage_grid_now = max(0, usage_now - production_now)

          co2 += usage_grid_now * get_carbon_intensity(date)

    return co2

def discover_exports ():
    paths = glob.glob(os.path.join(os.path.dirname(__file__), 'data/*json'))
    # Should hold a list of tuples:
    # (path, date)
    exports = []

    def get_date_from_path (path):
        m = re.match('.+data/(\d+)-(\d+).json', path)
        if m:
            month = int(m.group(1))
            day = int(m.group(2))

            return datetime.date(year=2022, month=month, day=day)
        else:
            return None
        
    for path in paths:
        date = get_date_from_path(path)
        exports.append((date, path))
    
    exports.sort(key=lambda e: e[0])

    return exports        

def calculate_carbon_till_now ():
    now = datetime.datetime.now()
    now_in_2022 = datetime.datetime(year=2022, month=now.month, day=now.day, hour=now.hour, minute=now.minute)
    today_in_2022 = datetime.date(year=2022, month=now.month, day=now.day)

    for (date, path) in discover_exports():
        if date == today_in_2022:
          data = solarweb.get_production_data(solarweb.load_export(path))
          return calculate_carbon_production_up_to(data, now_in_2022)
