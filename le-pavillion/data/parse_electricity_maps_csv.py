import csv
import datetime

# 0 - Date time
# 1 - Country
# 2 - Zone name
# 3 - Zone id
# 4 - Carbon intensity direct
# 5 - Carbon intensity LCA
# 6 - percentage low carbon
# 7 - percentage renewable

def carbon_intensity_key (date):
  return date.strftime('%d-%m-%H')

def read_carbon_intensity_data (path):
    data = {}

    with open(path, 'r') as csvfile:
        reader = csv.reader(csvfile)
        
        # Skip inital row
        next(reader)

        for row in reader:
            date = datetime.datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S')
            carbon_intensity = row[4]
            data[carbon_intensity_key(date)] = float(carbon_intensity)
            
    return data

"""
  Read carbon intensity data and return a function to get
  carbon instensity at a timestamp
"""
def load_carbon_intensities (path):
    data = read_carbon_intensity_data(path)
    
    def get_carbon_intenties(date):  
        return data[carbon_intensity_key(date)]
    
    return get_carbon_intenties