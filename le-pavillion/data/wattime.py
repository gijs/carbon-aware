import requests
from requests.auth import HTTPBasicAuth

baseurl = 'https://api.watttime.org'

username = ''
password = ''

# # First register
# # https://docs.watttime.org/#tag/Authentication/operation/post_username_register_post
# register_path = '/register'
# params = {'username': username,
#          'password': password,
#          'email': '',
#          'org': ''}
# rsp = requests.post(baseurl + register_path, json=params)
# print(rsp.text)

# Login to get auth token
# https://docs.watttime.org/#tag/Authentication/operation/get_token_login_get
login_path = '/login'
rsp = requests.get(baseurl + login_path, auth=HTTPBasicAuth(username, password))
TOKEN = rsp.json()['token']

# Get Region
# https://docs.watttime.org/#tag/GET-Regions-and-Maps/operation/get_reg_loc_v3_region_from_loc_get
headers = {"Authorization": f"Bearer {TOKEN}"}
params = {"latitude": "50.4782169", "longitude": "4.8554433", "signal_type": "co2_moer"}
region_path = '/v3/region-from-loc'
response = requests.get(baseurl + region_path, headers=headers, params=params)
response.raise_for_status()
print(response.json())
region = response.json()['region']

# Get intensity for the given date
# https://docs.watttime.org/#tag/GET-Historical

intensity_path = "/v3/historical"
# Provide your TOKEN here, see https://docs.watttime.org/#tag/Authentication/operation/get_token_login_get for more information
headers = {"Authorization": f"Bearer {TOKEN}"}
params = {
    "region": region,
    "start": "2022-05-23T00:00+00:00",
    "end": "2022-05-24T00:00+00:00",
    "signal_type": "co2_moer",
}
response = requests.get(baseurl + intensity_path, headers=headers, params=params)
response.raise_for_status()
print(response.json())
