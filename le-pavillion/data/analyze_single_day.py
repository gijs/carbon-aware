import datetime
from parse_electricity_maps_csv import load_carbon_intensities
import glob
import re
import solarweb
import csv
import hourly_consumption

"""
  Returns energy usage in kWh for a given time period at given date.
  In this example usage is linked to assumed opening hours 12:00 - 18:00
"""
def get_usage (timedelta, date):
    (hourly_consumption_day, hourly_consumption_night) = hourly_consumption.get_hourly_consumption(date.month)
    delta_hours = timedelta.total_seconds() / 3600

    if date.weekday () < 5:
        if date.hour < 7 or date.hour > 21:
            return hourly_consumption_night * delta_hours
        else:
            return hourly_consumption_day * delta_hours
        
    else:
        return hourly_consumption_night * delta_hours

get_carbon_intensity = load_carbon_intensities('BE_2022_hourly.csv') 

today_in_2022 = datetime.date(year=2022, month=5, day=30)

def analyze_production_data (production_data):
    # Production of solar in kWh
    production = 0
    # Time between data points
    timedelta = datetime.timedelta(minutes=5)
    timedelta_hours = timedelta.total_seconds() / 3600

    usage = {
       'total': 0,
       'solar': 0,
       'grid': 0
    }
    co2 = 0
    carbon_intensities = []

    for [date, production_now] in production_data:
        # Production is in kWh
        if production_now is None:
            production_now = 0

        production_now *= timedelta_hours
                
        # Assume the panel produces the same amount of energy until the next measurement
        production += production_now

        # Derive 
        usage_now = get_usage(timedelta, date)
        usage_solar_now = min(usage_now, production_now)
        usage_grid_now = max(0, usage_now - production_now)

        usage['total'] += usage_now
        usage['solar'] += usage_solar_now
        usage['grid'] += usage_grid_now

        carbon_intensities.append(get_carbon_intensity(date))
        co2 += usage_grid_now * carbon_intensities[-1]

    return {
       'production': production,
       'usage': usage,
       'co2': co2,
       'carbon_intensity': sum(carbon_intensities) / len(carbon_intensities)
    }

def discover_exports ():
    paths = glob.glob('data/*.json')
    # Should hold a list of tuples:
    # (path, date)
    exports = []

    def get_date_from_path (path):
        m = re.match('data/(\d+)-(\d+).json', path)
        if m:
            month = int(m.group(1))
            day = int(m.group(2))

            return datetime.date(year=2022, month=month, day=day)
        else:
            return None
        
    for path in paths:
        date = get_date_from_path(path)
        exports.append((date, path))
    
    exports.sort(key=lambda e: e[0])

    return exports        

if __name__ == '__main__':
    for (date, path) in discover_exports():
        if date == today_in_2022:
          data = solarweb.get_production_data(solarweb.load_export(path))
          analysis = analyze_production_data(data)

          print()
          print(date.strftime('%d-%m-%y'))
          print('Total production during day')
          print('{:.2f} kWh'.format(analysis['production']))

          print('Total energy used')
          print('{:.2f} kWh'.format(analysis['usage']['total']))

          print('Used solar power')
          print('{:.2f} kWh'.format(analysis['usage']['solar']))

          print('Used from the grid')
          print('{:.2f} kWh'.format(analysis['usage']['grid']))

          print('Average carbon intensity')
          print('{:.2f} gr'.format(analysis['carbon_intensity']))

          print('CO2 produced')
          print('{:.2f} gr'.format(analysis['co2']))
