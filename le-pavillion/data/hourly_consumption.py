import csv
import os.path


with open(os.path.join(os.path.dirname(__file__), 'derived_hourly_consumption.csv'), 'r') as csvfile:
    reader = csv.reader(csvfile)
    hourly_consumption_data = [(float(day), float(night)) for (day, night) in reader]

def get_hourly_consumption(month):
    return hourly_consumption_data[month-1]