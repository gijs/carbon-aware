# Script to run the installation
SERIAL=/dev/ttyACM0

# Trigger power plug init process on the raspberry.
ssh carbon-techno@carbon-techno.local bash 'steam_init.sh'


while :
do

  echo "Plotting powergrid"
  
  ssh carbon-techno@carbon-techno.local bash 'steam_on.sh'
  sleep 10  
  ssh carbon-techno@carbon-techno.local bash 'steam_off.sh'
	
  python generate_power_grid.py
  cp powergrid.svg plotter_scripts/
  cd plotter_scripts
  bash convert.sh powergrid.svg powergrid.gcode EMS_test.svg 
  echo "Plotting"
  python plot.py $SERIAL powergrid.gcode

  cd ..

  echo "Sleeping for two minutes"

  sleep 120

  echo "Plotting Carbon produced"

  ssh carbon-techno@carbon-techno.local bash 'steam_on.sh'
  sleep 10
  ssh carbon-techno@carbon-techno.local bash 'steam_off.sh'
  

  python generate_carbon_produced.py
  cp carbon-produced.svg plotter_scripts/carbon-produced.svg
  cd plotter_scripts
  bash convert.sh carbon-produced.svg carbon-produced.gcode EMS_test.svg 
  echo "Plotting"
  python plot.py $SERIAL carbon-produced.gcode

  echo "Sleeping for two minutes"

  sleep 120


  echo "Plotting solar production"

  ssh carbon-techno@carbon-techno.local bash 'steam_on.sh'
  sleep 10
  ssh carbon-techno@carbon-techno.local bash 'steam_off.sh'
  
  cd data
  python plot_installation.py
  cp solar-production.svg ../plotter_scripts/solar-production.svg
  cd ../plotter_scripts
  python remove_matplot_frame.py solar-production.svg solar-production-frameless.svg
  bash convert.sh solar-production-frameless.svg solar-production-frameless.gcode EMS_test.svg 
  echo "Plotting"
  python plot.py $SERIAL solar-production-frameless.gcode

  echo "Sleeping for two minutes"

  sleep 120

  echo "Plotting Carbon intensity"

  ssh carbon-techno@carbon-techno.local bash 'steam_on.sh'
  sleep 10
  ssh carbon-techno@carbon-techno.local bash 'steam_off.sh'
  
  python generate_carbon_intensity.py
  cp carbon-intensity.svg plotter_scripts/carbon-intensity.svg
  cd plotter_scripts
  bash convert.sh carbon-intensity.svg carbon-intensity.gcode EMS_test.svg 
  echo "Plotting"
  python plot.py $SERIAL carbon-intensity.gcode


  echo "Sleeping for two minutes"

  sleep 120

done