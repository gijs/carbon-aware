import subprocess
import json

# 
# node_modules/lighthouse/cli/index.js <url> --output="json" --quiet --disable-full-page-screenshot --chrome-flags="--headless --incognito"
#

LIGHTHOUSE_BINARY = "../node_modules/lighthouse/cli/index.js"

def lighthouse (url):
  args = [
    LIGHTHOUSE_BINARY,
    url,
    '--output="json"',
    '--quiet',
    '--disable-full-page-screenshot',
    '--chrome-flags="--headless --incognito"'
  ]

  try:
    return json.loads(subprocess.check_output(args, stderr=subprocess.STDOUT).decode())
  except subprocess.CalledProcessError as e:
    return 'Error:\n{}'.format(e.output.decode())
  
if __name__ == '__main__':
  print(lighthouse("http://example.com/"))