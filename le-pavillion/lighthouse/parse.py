import json

def pick_network_request_items (report):
  return report["audits"]["network-requests"]["details"]["items"]

def pick_resource_summary_items (report):
  return report["audits"]["resource-summary"]["details"]["items"]


def get_total_transfer_size (report):
  summaries = pick_resource_summary_items(report)

  for summary in summaries:
    if summary['resourceType'] == 'total':
      return summary['transferSize']


# with open('report-lighthouse.json') as h:
#   report = json.load(h)
#   network_requests = pick_network_request_items(report)
#   resource_total = 0
  
#   print(len(network_requests))

#   for request in network_requests:
#     print(request['url'])
#     print(request['transferSize'])
#     resource_total += request['transferSize']

#   resource_summary = pick_resource_summary_items(report)
#   print(resource_summary[0]['transferSize'] / 1000)
#   # Closest as to reported by console tools
#   print(resource_total / 1000)



  
