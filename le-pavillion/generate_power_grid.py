from lighthouse import electricity_maps
import math
import random
import svgwrite

lat = 50.47245336194251
lng = 4.862539272711904

"""
    {
      'zone': 'BE', 
      'datetime': '2024-05-30T09:00:00.000Z', 
      'updatedAt': '2024-05-30T09:50:06.514Z', 
      'createdAt': '2024-05-27T09:47:14.823Z', 
      'powerConsumptionBreakdown': {
          'nuclear': 4828, 
          'geothermal': 0, 
          'biomass': 117, 
          'coal': 0, 
          'wind': 708, 
          'solar': 2512, 
          'hydro': 416, 
          'gas': 359, 
          'oil': 6, 
          'unknown': 200, 
          'hydro discharge': 46, 
          'battery discharge': 0
      }, 
      'powerProductionBreakdown': {
          'nuclear': 3395, 
          'geothermal': None, 
          'biomass': 104, 
          'coal': None,
          'wind': 613,
          'solar': 2607,
          'hydro': 0,
          'gas': 387,
          'oil': 0,
          'unknown': 234,
          'hydro discharge': -166,
          'battery discharge': None
      },
      'powerImportBreakdown': {'DE': 0, 'FR': 3411, 'GB': 0, 'LU': 0, 'NL': 0},
      'powerExportBreakdown': {'DE': 692, 'FR': 0, 'GB': 176, 'LU': 148, 'NL': 377},
      'fossilFreePercentage': 94, 
      'renewablePercentage': 41, 
      'powerConsumptionTotal': 9192, 
      'powerProductionTotal': 7340, 
      'powerImportTotal': 3411, 
      'powerExportTotal': 1393, 
      'isEstimated': True, 
      'estimationMethod': 'TIME_SLICER_AVERAGE'
}
"""

powerSourceSymbols = {
    'nuclear': {'glyph': '¤', 'color': '' },
    'wind': {'glyph': '˜', 'color': '' },
    'gas': {'glyph': 'Ҩ', 'color': '' },
    'coal': {'glyph': '_', 'color': '' },
    'solar': {'glyph': '°', 'color': '' },
    'biomass': {'glyph': '-', 'color': '' },
    'oil': {'glyph': '¸', 'color': '' },
    'unknown': {'glyph': '¿', 'color': '' },
    'hydro': {'glyph': '~', 'color': '' },
    'geothermal': {'glyph': 'Ѡ', 'color': '' },
    'hydro discharge': {'glyph': '∫', 'color': '' },
    'battery discharge': {'glyph': '¬', 'color': '' }
}

powerbreakdown = electricity_maps.get_power_breakdown(lat, 
lng)

grid = (5, 5)
grid_size = grid[0] * grid[1]

consumption_total = powerbreakdown['powerConsumptionTotal']

glyphs = []
residual_glyphs = []

for source, consumption in powerbreakdown['powerConsumptionBreakdown'].items():
    glyph_count = (consumption / consumption_total) * grid_size
    glyphs_added = math.floor(glyph_count)
    residual_glyphs.append((powerSourceSymbols[source]['glyph'], glyph_count - glyphs_added))

    for _ in range(glyphs_added):
        glyphs.append(powerSourceSymbols[source]['glyph'])

# Reverse sort residual glyphs.
residual_glyphs.sort(key=lambda e: e[1])

while len(glyphs) < grid_size:
    glyphs.append(residual_glyphs.pop(-1)[0])

random.shuffle(glyphs)

svg_document = svgwrite.Drawing(filename="powergrid.svg", size = ("500px", "500px"))

margin = (50,75)
cell_size = (100,100)

for x in range(5):
    for y in range(5):
        glyph = glyphs.pop(-1)
        svg_document.add(svg_document.text(
            glyph,
            insert=(
                margin[0] + x * cell_size[0],
                margin[1] + y * cell_size[1]
            ),
            style='font-size: 75px; text-anchor: middle;'
        ))

svg_document.save()