from data.carbon_production import calculate_carbon_till_now
import svgwrite


svg_document = svgwrite.Drawing(filename="carbon-produced.svg", size = ("500px", "500px"))

svg_document.add(svg_document.text(
    'produced so far', insert=(250, 100), style='font-size: 50px; text-anchor: middle;'
))

svg_document.add(svg_document.text(
    int(calculate_carbon_till_now()), insert=(250, 300), style='font-size: 200px; text-anchor: middle;'
))

svg_document.add(svg_document.text(
    'grams of CO2', insert=(250, 400), style='font-size: 50px; text-anchor: middle;'
))


svg_document.save()