## data/analyze.py
Script which calculates carbon production per day using the production of the solar panel and the hourly carbon intensities.

## data/analyze_single_day.py
Script which calculates carbon production for a single day using the production of the solar panel and the hourly carbon intensities.

## data/calculate_hourly_consumption.py
Calculate an average usage per hour. Count week and weekend days per month to find hours at night and day rate. Do a simple division in the end.

## data/carbon_production.py
Functions to calculate carbon production for time periods.

## data/electricity_maps.py
Electricity maps API Wrapper

## data/hourly_consumption.py
Returns hourly consumption for given month, based on data in the derived_hourly_consumption.csv.

## data/parse_electricity_maps_csv.py
Functions to read the hourly_intensitities exports provided by electricity maps.

## data/plot_all.py
Generate plots for all solar production data and carbon intensity data.

## data/plot_installation.py
Plot solar production graph as used in the installation.

## data/plot.py
Generate styled plots for 30th of May 2022, intended to be printed.

## data/solarweb.py
Scripts to parse solarweb export data.

## data/wattime.py
Unused wrapper for wattime API.



## plotter_scripts/convert.sh

Utility script to convert SVG to gcode files.
First the text in the SVG is replaced by paths.
Then the file is cleaned and scaled with vpype.
Finally it transformed to gcode with juicy gcode.

## plotter_scripts/plot.py

Script to send a gcode file to a plotter, run like:

```
plot.py serialport gcodefile
```

## plotter_scripts/remove_matplot_frame.py
Utility to remove the backgroundframe matplotlib inserted in the svg.

```
remove_matplot_frame.py svgpathin svgpathout
```

## plotter_scripts/replace_text_with_paths.py

Script which replaces text elements in an svg with paths from the provided SVG-font

```
python replace_text_with_paths.py [--font fontpath] svgpathin svgpathout
```