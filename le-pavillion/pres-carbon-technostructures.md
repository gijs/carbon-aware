---
type: slide
slideOptions:
  transition: slide
  
---

<style>
    

    
  @font-face {
    font-family: 'Hershey-Noailles-Futura-Duplex-Regular';
    font-style: bold;
    font-weight: normal;
    src: local('Hershey-Noailles-Futura-Duplex-Regular'), local('Hershey-Noailles-Futura-Duplex-Regular'),
      url(http://hershey-noailles.luuse.io/output/Hershey-Noailles-Futura-Duplex-Regular/Hershey-Noailles-Futura-Duplex-Regular.woff) format('woff2');
  }
@font-face {
 font-family:"Hershey-Noailles-Futura-Duplex-Regular";
 src:local("Hershey-Noailles-Futura-Duplex-Regular"), local("Hershey-Noailles-Futura-Duplex-Regular"),
     url(http://hershey-noailles.luuse.io/output/Hershey-Noailles-Futura-Duplex-Regular/Hershey-Noailles-Futura-Duplex-Regular.ttf);
 font-weight:normal;
 font-style:normal
}
    
body {
    background-color: black;
   font-family: Hershey-Noailles-Futura-Simplex-Regular !important;
![](https://)}
h1, h2, h3, h4, h5, h6, p
{
    font-family: "Hershey-Noailles-Futura-Duplex-Regular", Helvetica, "Liberation Sans", Calibri, Arial, sans-serif; /* Sans-serif headers */

    /* font-family: "Hershey-Noailles-Futura-Duplex-Regular", "Georgia", "Times New Roman", serif; /* Serif headers */

    page-break-after: avoid; /* Firefox, Chrome, and Safari do not support the property value "avoid" */
}
    
    .disabled {
        color: grey;
    }
    
    pre     {
        text-align: left;
    }
    
.reveal h1,
.reveal h2,
.reveal h3,
.reveal h4,
.reveal h5,
.reveal h6,
.reveal blockquote,
.reveal {
	font-family: "Hershey-Noailles-Futura-Duplex-Regular";
	font-weight: normal !important;
}

.reveal {
	font-size: 36px;
}

.reveal p {
    font-family: "Hershey-Noailles-Futura-Duplex-Regular";
}
    
.reveal a {
    text-decoration: underline;
    color: inherit;
}
    
.reveal blockquote {
	width: initial;
	text-align: left;
	font-style: normal;
	font-size: 75%;
    padding: 0 1em;
}    

.reveal img {
    max-height: 80vh;
}    
    
</style>


# Carbon technostructures

---

The Start

---

https://caw.guillaumeslizewicz.com/carbon_aware/

---

# Le Pavillon Website

---

http://www.le-pavillon.be/

---

Comment calculer l'empreinte carbone d'un site?
How to calculate the carbon footprint of a website?

---

https://sustainablewebdesign.org/estimating-digital-emissions/

---

Nombre de visiteur/visitors: 2000 par mois

---

Weight of website (home): 645.09 KB

---

Pavillon namur homepage: 
    645.09 KB
    so one visit :   0.00064509x0,055kWh =0,00003547995
    in carbon (depending on the intensity of the grid ) =  0,00003547995x18= 0,6 mg of carbon

---

Running script of carbon calculation

---

<img style="filter:invert(100);" src="https://gitlab.constantvzw.org/gijs/carbon-aware/-/raw/main/le-pavillion/charts/carbon_calculus.svg?ref_type=heads">

---

Localisation website

---

<iframe src="https://www.google.com/maps/embed?pb=!4v1717071538399!6m8!1m7!1suRRIxoHdIWGcoSnZW7Fo9A!2m2!1d48.82197731563529!2d2.370749908383406!3f168.27!4f6.909999999999997!5f0.7820865974627469" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>>

---

# Le Pavillon Exhibition - biotopia

----

How to calculate carbon impact of an exhibition ?

---

Solar Pannels

---

<img style="filter:invert(100);" src="https://gitlab.constantvzw.org/gijs/carbon-aware/-/raw/main/le-pavillion/charts/solar-production-2022-05-30.svg">

---

<img style="filter:invert(100);" src="https://gitlab.constantvzw.org/gijs/carbon-aware/-/raw/main/le-pavillion/solar-all.svg?ref_type=heads">

---

Electric consumption

---

<img style="filter:invert(100);" src="https://gitlab.constantvzw.org/gijs/carbon-aware/-/raw/main/le-pavillion/charts/consumption-2022-05-30.svg?ref_type=heads">

---

Grid- the carbon intensity of the grid

---

<img style="filter:invert(100)" src="https://gitlab.constantvzw.org/gijs/carbon-aware/-/raw/main/le-pavillion/charts/carbon-intensity-2022-05-30.svg?ref_type=heads">

---

<img style="filter:invert(100)" src="https://gitlab.constantvzw.org/gijs/carbon-aware/-/raw/main/le-pavillion/intensity-all.svg?ref_type=heads">

---

Adding the two

---

<img style="filter:invert(100)" src="https://gitlab.constantvzw.org/gijs/carbon-aware/-/raw/main/le-pavillion/charts/carbon-intensity-2022-05-30.svg?ref_type=heads">

---

# How to plot on the cloud

---

Pen Plotter + steamer

---

Use of the lab for glass and mirror cutting and 3d printing

---

character for plotting -> hershey text generation to gcode

